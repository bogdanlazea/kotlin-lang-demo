# Kotlin Microservice Demo

## Requirements

IntelliJ Idea along with Kotlin plugin installed.

Gradle 6+ (use wrapper for builds)

Java 11 or 17 LTS

Postman or Insomnia

## Start the service

Start the application either from IntelliJ or using cmd `./gradlew bootRun` from root directory `kotlin-bookstore-manager`

## Performing requests

Swagger UI will be ready at http://localhost:8080/swagger-ui/index.html

If you prefer Postman for requests, click File -> Import -> Link and paste `http://localhost:8080/v3/api-docs`. This will add the entire request collection based on swagger file.

## Note

Same steps apply for `java-bookstore-manager` here the port number is set to `8081`

After import is complete you can start applying CRUD operations.

## Embedded H2 Database Console

Can be found on both instances at [H2 Console for Kotlin](http://localhost:8081/h2-console) or [H2 Console for Java](http://localhost:8080/h2-console)

JDBC url: `jdbc:h2:mem:jmanager` or `jdbc:h2:mem:ktmanager`

User Name: `sa`

Password: <leave empty>