package dtos

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class DtoExampleTest {

    @BeforeEach
    fun setUp() {
    }

    @AfterEach
    fun tearDown() {
    }

    @Test
    fun `maps User to UserResponse using extension function`() {
        val p = buildUser()
        val view = p.toUserView()
        assertUserView(view)
    }

    private fun buildUser(): UserEntity {
        return UserEntity(
            "Java",
            "Duke",
            "Javastreet",
            "42",
            "1234567",
            30,
            "s3cr37"
        )
    }

    private fun assertUserView(pr: UserDTO) {
        assertAll(
            { assertEquals("Java Duke", pr.name) },
            { assertEquals("Javastreet 42", pr.address) },
            { assertEquals("1234567", pr.telephone) },
            { assertEquals(30, pr.age) }
        )
    }
}
