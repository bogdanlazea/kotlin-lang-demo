package oop

class Car(var brand: String, var model: String, var year: Int, notAField: String){
}

fun main() {
    val c1 = Car("Ford", "Mustang", 1969, "not_a_field")
    println(c1.brand)
    //println(c1.notAField)

    val employee = Employee("bogdan", 30)
    employee.firstName
    //age is hidden
    //employee.age
}

class Employee constructor(firstName: String){ // a.k.a primary constructor

    val firstName = firstName;

    constructor(firstName: String, age: Int): this(firstName){ //secondary constructor
    }
}

//short form
// using val or var in constructors makes kotlin declare those fields as members of class
class EmployeeSimple(val firstName: String){}

class Demo {
    val dummy: String

    constructor() { // secondary constructor
        dummy = "Hello"
    }
}
