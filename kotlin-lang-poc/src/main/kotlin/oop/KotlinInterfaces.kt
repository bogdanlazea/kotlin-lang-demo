package oop

interface MyInterface {

    val test: Int

    fun foo() : String

    fun hello() {
        println("Hello there, pal!")
    }
}

class InterfaceImp : MyInterface {
    override val test: Int = 25
    override fun foo() = "Lol"
}


// EXAMPLE #2
interface A {

    fun callMe() {
        println("From interface A")
    }
}

interface B  {
    fun callMeToo() {
        println("From interface B")
    }
}

// implements two interfaces A and B
class Child: A, B



fun main(args: Array<String>) {
    val obj1 = InterfaceImp()

    println("test = ${obj1.test}")
    print("Calling hello(): ")

    obj1.hello()

    print("Calling and printing foo(): ")
    println(obj1.foo())


    val obj = Child()
    obj.callMe()
    obj.callMeToo()
}