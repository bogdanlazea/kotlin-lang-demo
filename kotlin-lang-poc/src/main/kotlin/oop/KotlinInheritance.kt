package oop

// open on the class means this class will allow derived classes
open class SuperClass  {

    val x = 5

    // no-open on a function means that
    // polymorphic behavior disabled if function overridden in derived class
    // In java is similar to final void disable() { }
    fun disable() { }

    // open on a function means that
    // polymorphic behavior allowed if function is overridden in derived class
    open fun animate() = println("super animate")
}

class ChildClass: SuperClass() {

    fun myFunction() {
        println(x) // x is now inherited from the superclass
    }

    // Explicit use of override keyword required to override a function in derived class
    override fun animate() = println("child animate")
}

fun main(args: Array<String>) {
    val ref = ChildClass()
    ref.myFunction()
    ref.animate()

    val superRef: SuperClass = ChildClass()
    superRef.animate()
    println(superRef.x)
}