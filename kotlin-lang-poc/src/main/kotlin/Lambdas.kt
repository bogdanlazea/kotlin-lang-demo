
// val lambdaName : Type = { argumentList -> codeBody }
// it is shorthand for single argument lambda

fun main() {
    val square = { number: Int -> number * number }

    val magnitude100String = { input : Int ->
        val magnitude = input * 100
        magnitude.toString()
    }

    val calculateGrade = { grade : Int ->
        when(grade) {
            in 0..40 -> "Fail"
            in 41..70 -> "Pass"
            in 71..100 -> "Distinction"
            else -> false
        }
    }

    //don't
    val calculateGradeAlt = fun(grade: Int): String {
        if (grade < 0 || grade > 100) {
            return "Error"
        } else if (grade < 40) {
            return "Fail"
        } else if (grade < 70) {
            return "Pass"
        }

        return "Distinction"
    }

    val nine = square(3)
    println(nine)
    println(magnitude100String(2))
    println(calculateGrade(171))

    //it example
    val array = arrayOf(1, 2, 3, 4, 5, 6)
    array.forEach { item -> println(item * 4) }

    //short
    array.forEach { println(it * 4) }

    val result1 = invokeLambda({ true }) // lambda literal

    //Another pattern for lambda literals encouraged by JetBrains – is to pass the lambda in as the last argument to a method and place
    // the lambda outside the method call
    val result2 = invokeLambda { arg -> arg.isNaN() }

    //Lambda Object Variable
    val result3 = { arg: Double -> arg == 4.329 }

    println(result1)
    println(result2)
}

fun invokeLambda(lambda: (Double) -> Boolean) : Boolean {
    return lambda(4.329)
}