package collections

// constructing maps default constructor
val iceCreamMap = LinkedHashMap<String, Int>()

// construct using factory function
val mapFactoryFunction = mapOf("Vanilla" to 24, "Chocolate" to 14, "Rocky Road" to 7)
val mapFactoryFunctionMutable = mutableMapOf("Vanilla" to 24, "Chocolate" to 14, "Rocky Road" to 7)

data class IceCreamShipment (val flavor: String, var quantity:Int)

val shipments = listOf(
    IceCreamShipment("Chocolate", 3),
    IceCreamShipment("Strawberry", 7),
    IceCreamShipment("Vanilla", 5),
    IceCreamShipment("Chocolate", 5),
    IceCreamShipment("Vanilla", 1),
    IceCreamShipment("Rocky Road", 10),
)

fun main(args: Array<String>) {
    //add vanilla to map
    iceCreamMap["Vanilla"] = 24

    val iceCreamInventory = mutableMapOf<String, Int>()

    for (shipment in shipments){
        val currentQuantity = iceCreamInventory[shipment.flavor] ?: 0
        iceCreamInventory[shipment.flavor] = currentQuantity + shipment.quantity
    }

    println("\n\nPopulated icecream inventory")
    for ((key, value) in iceCreamInventory)
        println("$key: $value")

    //same operations for put
    println(iceCreamInventory.get("Vanilla"))
    println(iceCreamInventory["Vanilla"])

    //To update an entry
    println("\n\nUpdate")
    val iceCreamSales = mutableMapOf("Chocolate" to 2, "Vanilla" to 3)
    iceCreamSales.merge("Chocolate", 1, Int::plus)
    println(iceCreamSales["Chocolate"])
    println(iceCreamSales["Vanilla"])

    // Remove vanilla and chocolate from sales
    println("\n\nRemoving")
    iceCreamSales.remove("Strawberry")
    iceCreamSales -= "Chocolate"
    println(iceCreamSales["Chocolate"])
    println(iceCreamSales["Vanilla"])

    //Filtering
    println("\n\nFiltering")
    val lotsLeft = iceCreamInventory.filterValues { qty -> qty > 5 }
    println(lotsLeft)

    //Mapping
    println("\n\nMapping")
    val asStrings = iceCreamInventory.map { (flavor, qty) -> "$qty tubs of $flavor" }
    println(asStrings)

    //For each
    println("\n\nFor each")
    iceCreamInventory.forEach{(key, value) -> println("$key: $value") }
}