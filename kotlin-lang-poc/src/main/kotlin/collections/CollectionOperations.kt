package collections

fun filterSamples() {
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val positives = numbers.filter { x -> x > 0 }
    val negatives = numbers.filter { it < 0 }

    println("Numbers: $numbers")
    println("Positive Numbers: $positives")
    println("Negative Numbers: $negatives")
}

fun mapSamples() {
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val doubled = numbers.map { x -> x * 2 }
    val tripled = numbers.map { it * 3 }

    println("Numbers: $numbers")
    println("Doubled Numbers: $doubled")
    println("Tripled Numbers: $tripled")
}

fun anySamples() {
    val numbers = listOf(1, -2, 3, -4, 5, -6)

    val anyNegative = numbers.any { it < 0 }

    val anyGT6 = numbers.any { it > 6 }

    println("Numbers: $numbers")
    println("Is there any number less than 0: $anyNegative")
    println("Is there any number greater than 6: $anyGT6")
}

fun allSamples() {
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val allEven = numbers.all { it % 2 == 0 }
    val allLess6 = numbers.all { it < 6 }

    println("Numbers: $numbers")
    println("All numbers are even: $allEven")
    println("All numbers are less than 6: $allLess6")
}

fun noneSamples() {
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val allEven = numbers.none { it % 2 == 1 }
    val allLess6 = numbers.none { it > 6 }

    println("Numbers: $numbers")
    println("All numbers are even: $allEven")
    println("No element greater than 6: $allLess6")
}

// if no such elements returns null
fun find_findLast_sample() {

    val words = listOf("Lets", "find", "something", "in", "collection", "somehow")

    val first = words.find { it.startsWith("some") }
    val last = words.findLast { it.startsWith("some") }

    val nothing = words.find { it.contains("nothing") }

    println("The first word starting with \"some\" is \"$first\"")
    println("The last word starting with \"some\" is \"$last\"")
    println("The first word containing \"nothing\" is ${nothing?.let { "\"$it\"" } ?: "null"}")
}

fun count() {

    val numbers = listOf(1, -2, 3, -4, 5, -6)

    val totalCount = numbers.count()
    val evenCount = numbers.count { it % 2 == 0 }

    println("Total number of elements: $totalCount")
    println("Number of even elements: $evenCount")
}

fun main() {
    filterSamples()
    mapSamples()
    anySamples()
    allSamples()
    noneSamples()
    find_findLast_sample()
    count()
}