fun main(args: Array<String>) {
    println("Hello World!")
    println("Program arguments: ${args.joinToString()}")


    var name = "John"

    // this is same as saying final Integer = 1975 - constant
    val birthyear = 1975

    println(name)
    println(birthyear)


    var firstName: String = "Doe";

    println("Hello " + firstName)

    println(sum(3, 5))

    val myNum = 5             // Int
    val myDoubleNum = 5.99    // Double
    val myFloat = 5.99f       // Float
    val myLetter = 'D'        // Char
    val myBoolean = true      // Boolean
    val myText = "Hello"      // String

    printFullName("Bogdan", "Alexandru")

    //if..else statements as expressions
    val time = 20
    var greeting = if (time < 18) {
        "Good day."
    } else {
        "Good evening."
    }
    println(greeting)

    greeting = if (time < 18) "Good day." else "Good evening."
}


fun sum(x: Int, y: Int): Int {
    return x + y
}

fun shortFromSum(x: Int, y: Int) = x + y

fun printFullName(firstName: String, lastName: String) {
    println("Full name is: ".plus("$firstName $lastName"))
}

fun getDayByNumber(day: Int){
    val result = when (day) {
        1 -> "Monday"
        2 -> "Tuesday"
        3 -> "Wednesday"
        4 -> "Thursday"
        5 -> "Friday"
        6 -> "Saturday"
        7 -> "Sunday"
        else -> "Invalid day."
    }
    println(result)
}

fun arrays(){
    val cars = arrayOf("Volvo", "BMW", "Ford", "Mazda")
    if ("Volvo" in cars) {
        println("It exists!")
    } else {
        println("It does not exist.")
    }

    // there is no traditional for loop in kotlin
    for (x in cars) {
        println(x)
    }

    // kotlin ranges
    //print the whole alphabet
    for (chars in 'a'..'x') {
        println(chars)
    }

    for (nums in 5..15) {
        println(nums)
    }
}

abstract class Car {
    abstract var type:String
}