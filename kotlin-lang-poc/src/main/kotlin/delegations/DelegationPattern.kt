package delegations

import java.util.*

interface SoundBehavior {                                                          // 1
    fun makeSound()
}

class ScreamBehavior(val n:String): SoundBehavior {                                // 2
    override fun makeSound() = println("${n.uppercase(Locale.getDefault())} !!!")
}

class RockAndRollBehavior(val n:String): SoundBehavior {                           // 2
    override fun makeSound() = println("I'm The King of Rock: $n")
}

class TomAraya(n:String): SoundBehavior by ScreamBehavior(n)                       // 3

class JimiHendrix(n:String): SoundBehavior by RockAndRollBehavior(n)              // 3

fun main() {
    val tomAraya = TomAraya("Thrash Metal")
    tomAraya.makeSound() // 4

    val jimiHendrix = JimiHendrix("Purple Haze")
    jimiHendrix.makeSound()
}