package delegations

//This uses the fact that a MutableMap is itself a delegate, allowing you to treat its keys as properties.
class DelegateExample(map: MutableMap<String, Any?>) {
    var name: String by map
}

//The lazy delegate allows the value of a property to be computed only on first access and then cached.
//This can be useful for properties that might be expensive to compute and that you might not ever need – for example, is loaded from a database


// Defining an immutable variable in kotlin
val lazyValue: String by lazy {
    println("I am running only once")
    "value" // set this as value for lazyvalue
}

fun main() {
    println(lazyValue)
    println("-------------------")
    println(lazyValue)
}

