package basics

val cars = arrayOf("Volvo", "BMW", "Ford", "Mazda")

fun main(args: Array<String>) {
    println(cars[0])
    cars[0] = "Opel"
    println("It has now changed to: " + cars[0])

    println(cars.size)

    if ("BMW" in cars) {
        println("BMW it exists!")
    } else {
        println("BMW it does not exist.")
    }

    for (x in cars) {
        println(x)
    }
}