package basics

fun main(args: Array<String>) {
    basicIfElse()
    alternativeToIfElse()
}

fun basicIfElse() {
    if (20 > 18) {
        println("20 is greater than 18")
    }

    val time = 20
    if (time < 18) {
        println("Good day.")
    } else {
        println("Good evening.")
    }


    if (time < 30) {
        println("Good morning.")
    } else if (time < 20) {
        println("Good day.")
    } else {
        println("Good evening.")
    }
}

//Java switch equivalent
fun alternativeToIfElse() {
    val day = 4

    val result = when (day) {
        1 -> "Monday"
        2 -> "Tuesday"
        3 -> "Wednesday"
        4 -> "Thursday"
        5 -> "Friday"
        6 -> "Saturday"
        7 -> "Sunday"
        else -> "Invalid day."
    }
    println(result)
}