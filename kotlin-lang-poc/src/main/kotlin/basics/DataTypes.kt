package basics

val myNum = 5             // Int
var l = 6L;
val myDoubleNum = 5.99    // Double
val myLetter = 'D'        // Char
val myBoolean = true      // Boolean
val myText: String = "'fga' "   // String

fun main(args: Array<String>) {
    stringsDemo()
    compareStrings()
    concatenation()
}

fun stringsDemo() {
    var txt = "Hello World"
    println(txt[0]) // first element (H)
    println(txt[2]) // third element (l)

    println("The length of the txt string is: " + txt.length)

    println(txt.toUpperCase())   // Outputs "HELLO WORLD"
    println(txt.toLowerCase())   // Outputs "hello world"

    txt = "Please locate where 'locate' occurs!"
    println(txt.indexOf("locate"))  // Outputs 7
}

fun compareStrings() {
    var txt1 = "Hello World"
    var txt2 = "Hello World"
    println(txt1.compareTo(txt2))  // Outputs 0 (they are equal)
}

fun concatenation() {
    var firstName = "John"
    var lastName = "Doe"
    println(firstName + " " + lastName)
    println(firstName.plus(lastName))

    //using interpolation
    println("My name is $firstName $lastName")
}

fun boolDemo() {
    val isKotlinFun = true
    val isFishTasty = false
    println(isKotlinFun)   // Outputs true
    println(isFishTasty)   // Outputs false
}