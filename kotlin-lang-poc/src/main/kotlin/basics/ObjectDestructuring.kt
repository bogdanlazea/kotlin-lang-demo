package basics


data class Person(var id: Int, var name: String, var age: Int, var l:String)


fun main() {
    val person = Person(1, "Jon Snow", 20, "rabndom")
    val(id, name, age) = person

    println(id)     //1
    println(name)   //Jon Snow
    println(age)    //20
}