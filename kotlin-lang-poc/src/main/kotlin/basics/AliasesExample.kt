package basics

typealias EmployerSet = MutableSet<Employee>

data class Employee(var name: String, val id: Int) // {} are redundant if no extra fields, methods

fun main(args: Array<String>) {
    val employer1 = Employee("Lynn Jones", 500)
    val employer2 = Employee("John Doe", 500)

    val employers: EmployerSet = mutableSetOf()

    employers.add(employer1)
    employers.add(employer2)
    println(employers)
}

