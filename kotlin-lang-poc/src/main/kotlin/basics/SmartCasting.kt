package basics

typealias EmployeeSetx = Set<Employeex>

fun main(args: Array<String>)  {

    val employeeOne = Employeex("Mary", 1)
    val employeeTwo = Employeex("John", 2)
    val employeeThree = Employeex("John", 2)

    println(employeeOne === employeeTwo)
    println(employeeTwo === employeeThree)
    println(employeeOne == employeeTwo)
    println(employeeTwo == employeeThree)

    val employeeFour = employeeTwo
    println(employeeFour === employeeTwo)

    println(employeeFour != employeeTwo)
    println(employeeFour !== employeeTwo)
    println(employeeTwo != employeeThree)
    println(employeeTwo !== employeeThree)


    val x = 0x00101101
    val y = 0x11011011

    var something: Any = employeeFour
    if (something is Employeex) {
        //val newEmployee = something as Employeex
        something = employeeOne
        println(something.name)
    }


//    var number: Int
//    number = 10;
//    number = 20
//
//    val names = arrayListOf("John", "Jane", "Mary")
//    println(names[1])
//
//    val employees: EmployeeSetx
//
//    val employee1 = Employeex("Lynn Jones", 500)
//    employee1.name = "Lynn Smith"
//
//    val employee2: Employeex
//    val number2 = 100
//
//    if (number < number2) {
//        employee2 = Employeex("Jane Smith", 400)
//    }
//    else {
//        employee2 = Employeex("Mike Watson", 150)
//    }
}

class Employeex(var name: String, val id : Int) {

    override fun equals(obj: Any?): Boolean {
        if (obj is Employeex) {
            return name == obj.name && id == obj.id
        }

        return false
    }

}
