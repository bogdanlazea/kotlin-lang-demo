package basics

fun main(args: Array<String>) {
    basicForLoop()
    kotlinRanges()

    val nums = arrayOf(2, 4, 6, 8)
    containsValue(2, nums)

    breakExample()
}

//Unlike Java and other programming languages, there is no traditional for loop in Kotlin.
fun basicForLoop() {
    val cars = arrayOf("Volvo", "BMW", "Ford", "Mazda")
    for (x in cars) {
        println(x)
    }
}

fun kotlinRanges() {
    for (chars in 'a'..'x') {
        println(chars)
    }

    for (nums in 5..15) {
        println(nums)
    }
}

fun containsValue(value: Int, nums: Array<Int>){
    if (value in nums) {
        println("It exists!")
    } else {
        println("It does not exist.")
    }
}

fun breakExample() {
    for (nums in 5..15) {
        if (nums == 10) {
            break
        }
        println(nums)
    }
}

fun skipExample(): Unit { // same as returning void in java, can be omitted
    for (nums in 5..15) {
        if (nums == 10) {
            continue
        }
        println(nums)
    }
}