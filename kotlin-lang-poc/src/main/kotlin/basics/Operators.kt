package basics

fun main(args: Array<String>) {
    var x = 5
    var y = 3
    println(x >= y) // returns true because five is either greater than, or equal, to 3
}