package dtos

class UserEntity(
    val firstName: String,
    val lastName: String,
    val street: String,
    val houseNumber: String,
    val phone: String,
    val age: Int,
    val password: String)

data class UserDTO(
    val name: String,
    val address: String,
    val telephone: String,
    val age: Int
)

// Enhance UserEntity with toUserView() convertor method
fun UserEntity.toUserView() = UserDTO(
    name = "$firstName $lastName",
    address = "$street $houseNumber",
    telephone = phone,
    age = age
)
