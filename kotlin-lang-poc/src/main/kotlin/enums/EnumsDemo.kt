package enums

enum class ShipmentUnit {
    LBS,
    KGS,
    BOXES
}

