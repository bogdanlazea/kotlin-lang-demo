package org.bookstore.jmanager;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class HomeController {

	@GetMapping("/")
	@ResponseBody
	public String index() {
		return "Springboot with Java 11 POC";
	}

}
