package org.bookstore.jmanager.model;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BookDTO {

	private Long id;

	@NotNull
	@Size(max = 255)
	private String title;

	@NotNull
	private BookStatus status;

	private List<Long> authors;

}
