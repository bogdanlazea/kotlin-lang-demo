package org.bookstore.jmanager.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AuthorDTO {

	private Long id;

	@NotNull
	@Size(max = 255)
	private String firstName;

	@NotNull
	@Size(max = 255)
	private String lastName;

	@NotNull
	@Size(max = 255)
	private String email;

}
