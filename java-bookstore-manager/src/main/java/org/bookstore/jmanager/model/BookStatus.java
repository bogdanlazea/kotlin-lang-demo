package org.bookstore.jmanager.model;


public enum BookStatus {

	IN_STORE,
	SOLD_OUT,
	ARRIVING_SOON

}
