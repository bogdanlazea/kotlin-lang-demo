package org.bookstore.jmanager.rest;

import java.util.List;

import javax.validation.Valid;

import org.bookstore.jmanager.model.AuthorDTO;
import org.bookstore.jmanager.service.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/authors", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthorController {

	private final AuthorService authorService;

	public AuthorController(final AuthorService authorService) {
		this.authorService = authorService;
	}

	@GetMapping
	public ResponseEntity<List<AuthorDTO>> getAllAuthors() {
		return ResponseEntity.ok(authorService.findAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<AuthorDTO> getAuthor(@PathVariable final Long id) {
		return ResponseEntity.ok(authorService.get(id));
	}

	@PostMapping
	public ResponseEntity<Long> createAuthor(@RequestBody @Valid final AuthorDTO authorDTO) {
		return new ResponseEntity<>(authorService.create(authorDTO), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Void> updateAuthor(@PathVariable final Long id,
			@RequestBody @Valid final AuthorDTO authorDTO) {
		authorService.update(id, authorDTO);
		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAuthor(@PathVariable final Long id) {
		authorService.delete(id);
		return ResponseEntity.noContent().build();
	}

}
