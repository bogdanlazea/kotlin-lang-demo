package org.bookstore.jmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class JmanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmanagerApplication.class, args);
	}

}
