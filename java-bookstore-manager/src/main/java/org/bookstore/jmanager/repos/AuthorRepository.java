package org.bookstore.jmanager.repos;

import org.bookstore.jmanager.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AuthorRepository extends JpaRepository<Author, Long> {
}
