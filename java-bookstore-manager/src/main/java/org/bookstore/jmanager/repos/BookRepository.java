package org.bookstore.jmanager.repos;

import org.bookstore.jmanager.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BookRepository extends JpaRepository<Book, Long> {
}
