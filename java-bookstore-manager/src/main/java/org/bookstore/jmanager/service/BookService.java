package org.bookstore.jmanager.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.bookstore.jmanager.domain.Author;
import org.bookstore.jmanager.domain.Book;
import org.bookstore.jmanager.model.BookDTO;
import org.bookstore.jmanager.repos.AuthorRepository;
import org.bookstore.jmanager.repos.BookRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Transactional
@Service
public class BookService {

	private final BookRepository bookRepository;
	private final AuthorRepository authorRepository;

	public BookService(final BookRepository bookRepository,
			final AuthorRepository authorRepository) {
		this.bookRepository = bookRepository;
		this.authorRepository = authorRepository;
	}

	public List<BookDTO> findAll() {
		return bookRepository.findAll()
				.stream()
				.map(book -> mapToDTO(book, new BookDTO()))
				.collect(Collectors.toList());
	}

	public BookDTO get(final Long id) {
		return bookRepository.findById(id)
				.map(book -> mapToDTO(book, new BookDTO()))
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	public Long create(final BookDTO bookDTO) {
		final Book book = new Book();
		mapToEntity(bookDTO, book);
		return bookRepository.save(book).getId();
	}

	public void update(final Long id, final BookDTO bookDTO) {
		final Book book = bookRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		mapToEntity(bookDTO, book);
		bookRepository.save(book);
	}

	public void delete(final Long id) {
		bookRepository.deleteById(id);
	}

	private BookDTO mapToDTO(final Book book, final BookDTO bookDTO) {
		bookDTO.setId(book.getId());
		bookDTO.setTitle(book.getTitle());
		bookDTO.setStatus(book.getStatus());
		bookDTO.setAuthors(book.getBookAuthors() == null ? null : book.getBookAuthors().stream()
				.map(Author::getId)
				.collect(Collectors.toList()));
		return bookDTO;
	}

	private Book mapToEntity(final BookDTO bookDTO, final Book book) {
		book.setTitle(bookDTO.getTitle());
		book.setStatus(bookDTO.getStatus());
		if (bookDTO.getAuthors() != null) {
			final List<Author> authors = authorRepository.findAllById(bookDTO.getAuthors());
			if (authors.size() != bookDTO.getAuthors().size()) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "one of the authors not found");
			}
			book.setBookAuthors(authors.stream().collect(Collectors.toSet()));
		}
		return book;
	}

}
