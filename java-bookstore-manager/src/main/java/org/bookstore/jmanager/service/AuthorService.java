package org.bookstore.jmanager.service;

import java.util.List;
import java.util.stream.Collectors;

import org.bookstore.jmanager.domain.Author;
import org.bookstore.jmanager.model.AuthorDTO;
import org.bookstore.jmanager.repos.AuthorRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;


@Service
public class AuthorService {

	private final AuthorRepository authorRepository;

	public AuthorService(final AuthorRepository authorRepository) {
		this.authorRepository = authorRepository;
	}

	public List<AuthorDTO> findAll() {
		return authorRepository.findAll()
				.stream()
				.map(author -> mapToDTO(author, new AuthorDTO()))
				.collect(Collectors.toList());
	}

	public AuthorDTO get(final Long id) {
		return authorRepository.findById(id)
				.map(author -> mapToDTO(author, new AuthorDTO()))
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	public Long create(final AuthorDTO authorDTO) {
		final Author author = new Author();
		mapToEntity(authorDTO, author);
		return authorRepository.save(author).getId();
	}

	public void update(final Long id, final AuthorDTO authorDTO) {
		final Author author = authorRepository.findById(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
		mapToEntity(authorDTO, author);
		authorRepository.save(author);
	}

	public void delete(final Long id) {
		authorRepository.deleteById(id);
	}

	private AuthorDTO mapToDTO(final Author author, final AuthorDTO authorDTO) {
		authorDTO.setId(author.getId());
		authorDTO.setFirstName(author.getFirstName());
		authorDTO.setLastName(author.getLastName());
		authorDTO.setEmail(author.getEmail());
		return authorDTO;
	}

	private Author mapToEntity(final AuthorDTO authorDTO, final Author author) {
		author.setFirstName(authorDTO.getFirstName());
		author.setLastName(authorDTO.getLastName());
		author.setEmail(authorDTO.getEmail());
		return author;
	}

}
