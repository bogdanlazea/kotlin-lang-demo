package org.bookstore.ktmanager.model

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size


data class BookDTO(
    var id: Long? = null,
    @get:NotNull
    @get:Size(max = 255)
    var title: String? = null,
    @get:NotNull
    var status: BookStatus? = null,
    var authors: List<Long>? = null
)
