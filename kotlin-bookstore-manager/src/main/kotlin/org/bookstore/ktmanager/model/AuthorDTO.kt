package org.bookstore.ktmanager.model

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size


data class AuthorDTO(
    var id: Long? = null,
    @get:NotNull
    @get:Size(max = 255)
    var firstName: String? = null,
    @get:NotNull
    @get:Size(max = 255)
    var lastName: String? = null,
    @get:NotNull
    @get:Size(max = 255)
    var email: String? = null
)
