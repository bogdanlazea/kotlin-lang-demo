package org.bookstore.ktmanager.model


enum class BookStatus {

    IN_STORE,
    SOLD_OUT,
    ARRIVING_SOON

}
