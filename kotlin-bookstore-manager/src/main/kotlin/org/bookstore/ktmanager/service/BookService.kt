package org.bookstore.ktmanager.service

import javax.transaction.Transactional
import kotlin.streams.toList
import org.bookstore.ktmanager.domain.Author
import org.bookstore.ktmanager.domain.Book
import org.bookstore.ktmanager.model.BookDTO
import org.bookstore.ktmanager.repos.AuthorRepository
import org.bookstore.ktmanager.repos.BookRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException


@Transactional
@Service
class BookService(
    private val bookRepository: BookRepository,
    private val authorRepository: AuthorRepository
) {

    fun findAll(): List<BookDTO> {
        return bookRepository.findAll()
                .stream()
                .map { book -> mapToDTO(book, BookDTO()) }
                .toList()
    }

    fun `get`(id: Long): BookDTO {
        return bookRepository.findById(id)
                .map { book -> mapToDTO(book, BookDTO()) }
                .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    fun create(bookDTO: BookDTO): Long {
        val book = Book()
        mapToEntity(bookDTO, book)
        return bookRepository.save(book).id!!
    }

    fun update(id: Long, bookDTO: BookDTO) {
        val book: Book = bookRepository.findById(id)
                .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
        mapToEntity(bookDTO, book)
        bookRepository.save(book)
    }

    fun delete(id: Long) {
        bookRepository.deleteById(id)
    }

    fun mapToDTO(book: Book, bookDTO: BookDTO): BookDTO {
        bookDTO.id = book.id
        bookDTO.title = book.title
        bookDTO.status = book.status
        bookDTO.authors = book.bookAuthors?.stream()
                ?.map { author -> author.id!! }
                ?.toList()
        return bookDTO
    }

    fun mapToEntity(bookDTO: BookDTO, book: Book): Book {
        book.title = bookDTO.title
        book.status = bookDTO.status
        if (bookDTO.authors != null) {
            val authors: List<Author> = authorRepository.findAllById(bookDTO.authors!!)
            if (authors.size != bookDTO.authors!!.size) {
                throw ResponseStatusException(HttpStatus.NOT_FOUND, "one of authors not found")
            }
            book.bookAuthors = authors.toMutableSet()
        }
        return book
    }

}
