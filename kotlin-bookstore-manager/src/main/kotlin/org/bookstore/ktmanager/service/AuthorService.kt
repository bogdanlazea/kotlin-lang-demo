package org.bookstore.ktmanager.service

import kotlin.streams.toList
import org.bookstore.ktmanager.domain.Author
import org.bookstore.ktmanager.model.AuthorDTO
import org.bookstore.ktmanager.repos.AuthorRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException


@Service
class AuthorService(
    private val authorRepository: AuthorRepository
) {

    fun findAll(): List<AuthorDTO> {
        return authorRepository.findAll()
                .stream()
                .map { author -> mapToDTO(author, AuthorDTO()) }
                .toList()
    }

    fun `get`(id: Long): AuthorDTO {
        return authorRepository.findById(id)
                .map { author -> mapToDTO(author, AuthorDTO()) }
                .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    fun create(authorDTO: AuthorDTO): Long {
        val author = Author()
        mapToEntity(authorDTO, author)
        return authorRepository.save(author).id!!
    }

    fun update(id: Long, authorDTO: AuthorDTO) {
        val author: Author = authorRepository.findById(id)
                .orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
        mapToEntity(authorDTO, author)
        authorRepository.save(author)
    }

    fun delete(id: Long) {
        authorRepository.deleteById(id)
    }

    fun mapToDTO(author: Author, authorDTO: AuthorDTO): AuthorDTO {
        authorDTO.id = author.id
        authorDTO.firstName = author.firstName
        authorDTO.lastName = author.lastName
        authorDTO.email = author.email
        return authorDTO
    }

    fun mapToEntity(authorDTO: AuthorDTO, author: Author): Author {
        author.firstName = authorDTO.firstName
        author.lastName = authorDTO.lastName
        author.email = authorDTO.email
        return author
    }

}
