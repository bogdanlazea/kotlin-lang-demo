package org.bookstore.ktmanager

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class KtmanagerApplication

fun main(args: Array<String>) {
    runApplication<KtmanagerApplication>(*args)
}
