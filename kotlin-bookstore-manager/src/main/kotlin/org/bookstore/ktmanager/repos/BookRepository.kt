package org.bookstore.ktmanager.repos

import org.bookstore.ktmanager.domain.Book
import org.springframework.data.jpa.repository.JpaRepository


interface BookRepository : JpaRepository<Book, Long>
