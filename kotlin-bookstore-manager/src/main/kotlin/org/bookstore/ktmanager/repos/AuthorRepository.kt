package org.bookstore.ktmanager.repos

import org.bookstore.ktmanager.domain.Author
import org.springframework.data.jpa.repository.JpaRepository


interface AuthorRepository : JpaRepository<Author, Long>
