package org.bookstore.ktmanager.rest

import org.bookstore.ktmanager.model.BookDTO
import org.bookstore.ktmanager.service.BookService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping(
    value = ["/api/books"],
    produces = [MediaType.APPLICATION_JSON_VALUE]
)
class BookController(private val bookService: BookService) {

    @GetMapping
    fun getAllBooks(): ResponseEntity<List<BookDTO>> = ResponseEntity.ok(bookService.findAll())

    @GetMapping("/{id}")
    fun getBook(@PathVariable id: Long): ResponseEntity<BookDTO> = ResponseEntity.ok(bookService.get(id))

    @PostMapping
    fun createBook(@RequestBody @Valid bookDTO: BookDTO): ResponseEntity<Long> =
        ResponseEntity(bookService.create(bookDTO), HttpStatus.CREATED)

    @PutMapping("/{id}")
    fun updateBook(@PathVariable id: Long, @RequestBody @Valid bookDTO: BookDTO): ResponseEntity<Void> {
        bookService.update(id, bookDTO)
        return ResponseEntity.ok().build()
    }

    @DeleteMapping("/{id}")
    fun deleteBook(@PathVariable id: Long): ResponseEntity<Void> {
        bookService.delete(id)
        return ResponseEntity.noContent().build()
    }

}
