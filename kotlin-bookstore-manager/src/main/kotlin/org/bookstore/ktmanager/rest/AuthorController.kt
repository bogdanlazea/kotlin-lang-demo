package org.bookstore.ktmanager.rest

import org.bookstore.ktmanager.model.AuthorDTO
import org.bookstore.ktmanager.service.AuthorService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping(
    value = ["/api/authors"],
    produces = [MediaType.APPLICATION_JSON_VALUE]
)
class AuthorController(private val authorService: AuthorService) {

    @GetMapping
    fun getAllAuthors(): ResponseEntity<List<AuthorDTO>> = ResponseEntity.ok(authorService.findAll())

    @GetMapping("/{id}")
    fun getAuthor(@PathVariable id: Long): ResponseEntity<AuthorDTO> = ResponseEntity.ok(authorService.get(id))

    @PostMapping
    fun createAuthor(@RequestBody @Valid authorDTO: AuthorDTO): ResponseEntity<Long> =
        ResponseEntity(authorService.create(authorDTO), HttpStatus.CREATED)

    @PutMapping("/{id}")
    fun updateAuthor(@PathVariable id: Long, @RequestBody @Valid authorDTO: AuthorDTO): ResponseEntity<Void> {
        authorService.update(id, authorDTO)
        return ResponseEntity.ok().build()
    }

    @DeleteMapping("/{id}")
    fun deleteAuthor(@PathVariable id: Long): ResponseEntity<Void> {
        authorService.delete(id)
        return ResponseEntity.noContent().build()
    }

}
